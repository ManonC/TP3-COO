class Box :

	def __init__ (self):
		self._contents = []

	def add (self, truc):
		self._contents.append(truc)

	def is_in(self, truc):
		return truc in self._contents

	def rmv(self, truc):
		self._contents.remove(truc)

	def __contains__ (self, truc):
		return self.is_in(truc)

