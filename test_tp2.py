from tp2 import *

#créer boites
def test_creationBoite():

	b = Box()


#mettre objets dans boites
def test_ajoutObjetDansBoite():
	b = Box()
	b.add("truc")
	b.add("truc2")

#objet dans la boite
def test_ObjetAjoute():
	b = Box()
	b.add("truc1")
	b.add("truc2")
	assert b.is_in("truc1")
	assert b.is_in("truc2")

#objets manquant dans la boite
def test_pasObjetDansBoite():

	b = Box()
	assert not b.is_in("truc1")
	b.add("truc1")
	assert not b.is_in("truc2")

#contents doit etre privé : _contents

#tester facilement si un truc est dans une boite
def test_trucDansBoite():
	b = Box()
	assert "truc1" not in b
	b.add("truc1")
	assert "truc1" in b

#retirer objet de la boite
def test_objetRetire():
	b = Box()
	b.add("truc1")
	b.add("truc2")
	assert "truc1" in b
	assert "truc2" in b
	b.rmv("truc2")
	assert "truc2" not in b
